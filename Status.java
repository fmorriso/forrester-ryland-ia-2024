public class Status
{
    private int Tank_HP = 100;
    public int getTank_HP() 
    {
        return Tank_HP;
    }

    public void setTank_HP(int Tank_HP)
    {
        this.Tank_HP = Tank_HP;
    }

    private int parts = 1000;
    public int getparts() 
    {
        return parts;
    }

    public void setparts(int parts)
    {
        this.parts = parts;
    }

    private int current_day = 0;
    public int getcurrent_day() 
    {
        return current_day;
    }

    public void setcurrent_day(int current_day)
    {
        this.current_day = current_day;
    }

    private int daysLeft = 7;
    public int getdaysLeft() 
    {
        return daysLeft;
    }

    public void setdaysleft(int daysLeft)
    {
        this.daysLeft = daysLeft;
    }

    private int distanceLeft = 1692;
    public int getdistanceLeft() 
    {
        return distanceLeft;
    }

    public void setdistanceLeft(int distanceLeft)
    {
        this.distanceLeft = distanceLeft;
    }
    
    private int distanceTraveled = 1;
    public int getdistanceTraveled()
    {
        return distanceTraveled;
    }

    public void setdistanceTraveled(int distanceTraveled)
    {
        this.distanceTraveled = distanceTraveled;
    }

    private int hoursindayLeft = 16;
    public int gethoursindayLeft() 
    {
        return hoursindayLeft;
    }

    public void sethoursindayLeft(int hoursindayLeft)
    {
        this.hoursindayLeft = hoursindayLeft;
    }

    private int hoursspent = 0;
    public int gethoursspent()
    {
        return hoursspent;
    }

    public void sethoursspent(int hoursspent)
    {
        this.hoursspent = hoursspent;
    }

    private int totalhoursspent = 0;
    public int gettotalhoursspent()
    {
        return totalhoursspent;
    }

    public void settotalhoursspent(int totalhoursspent)
    {
        this.totalhoursspent = totalhoursspent;
    }
    
    private double tank_fuel = 100;
    public double gettank_fuel() 
    {
        return tank_fuel;
    }

    public void settank_fuel(double tank_fuel)
    {
        this.tank_fuel = tank_fuel;
    }
    
    private int ticks_in_day = hoursindayLeft*4;
    public int getticks_in_day() 
    {
        return ticks_in_day;
    }

    public void setticks_in_day(int ticks_in_day)
    {
        this.ticks_in_day = ticks_in_day;
    }



    public void displayStatus()
    {
        System.out.format("Tank HP is at: %d%n. ", Tank_HP);
        System.out.format("Fuel reserves are at: %s. ", tank_fuel);
        System.out.format("Remaining Kgs of parts are: %s. ", parts);
        System.out.format("Current day is: %s. ", current_day);
        System.out.format("Distance left to travel: %s. ", distanceLeft);
    }
}