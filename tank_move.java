public class tank_move
{
    private executable exec;
    private Status game_status;
    private Game_window game_window;


    public tank_move(executable program)
    {
        exec = program;
    }

    public void TankMethodThatDoesSomething()
    {
        int ibt = exec.getGameStatus().gettotalhoursspent() * 34;
        game_status.setdistanceTraveled(ibt);
        System.out.println("Move for X hours");
        int hoursUsed = 0;
        hoursUsed = Integer.parseUnsignedInt(game_window.getinputValue(), hoursUsed);
        game_status.sethoursspent(hoursUsed);
        game_status.settank_fuel(game_status.gettank_fuel() - hoursUsed * 6.2);
        game_status.sethoursindayLeft(-hoursUsed);
        if (hoursUsed >= 17) {
            System.out.println("Oops you did the forbidden. Exiting program.");
            System.exit(0);
        } else {
            game_status.setcurrent_day(+1);
            game_status.sethoursindayLeft(16);
            System.out.println("The next day is here!");
            game_status.sethoursspent(0);
        }
    }
}
